package tk.hugo4715.rouedudestin.gui;

import org.apache.commons.lang.Validate;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.AbstractGui;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class RDDGui extends AbstractGui {

	public RDDGui(Player player) {
		super(RDD.get(), player, "Roue Du destin", 3*9, Integer.MAX_VALUE);
	}
	
	@Override
	public void update() {
		//fill background
		for(int i = 0; i < inv.getSize();i++){
			inv.setItem(i, new ItemFactory(Material.STAINED_GLASS_PANE).withName(" ").withColor(DyeColor.WHITE).done());
		}
		
		if(!RDD.get().getTargets().containsKey(player)){
			RDD.get().changeTarget(player);
		}
		
		Player target = RDD.get().getTargets().get(player);
		System.out.println("target: " + target);
		buttons.clear();
		
		//open GuiRoue button
		if(target != null){
			inv.setItem(9 + 3, new ItemFactory(Material.SKULL_ITEM).withOwner(ChatColor.GOLD + target.getName()).done());
			buttons.put(9 + 3, new ButtonClickListener() {
				
				@Override
				public void onClick(int slot) {
					stop();
					Validate.notNull(RDD.get().getTargets().get(player));
					Validate.notNull(player);
					new GuiRoue(player,RDD.get().getTargets().get(player));//TODO find a better name
				}
			});
		}else{
			inv.setItem(9 + 3, new ItemFactory(Material.BARRIER).withName(ChatColor.RED + "Aucun joueur trouv�").done());
		}
		
		
		inv.setItem(9 + 5, new ItemFactory(Material.ARROW).withName("Changer").done());
		buttons.put(9 + 5, new ButtonClickListener() {
			
			@Override
			public void onClick(int slot) {
				//update target and update gui content
				RDD.get().changeTarget(player);
				update();
			}
		});
		
	}

}
