package tk.hugo4715.rouedudestin.gui;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.destin.Destin;
import tk.hugo4715.rouedudestin.destin.Destin.Type;
import tk.hugo4715.rouedudestin.util.AbstractGui;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class GuiRoue extends AbstractGui {

	private static ArrayList<Destin> destins = new ArrayList<Destin>();


	static {
		for(Type t : Destin.Type.values()){
			try {
				Destin dest = t.getclz().newInstance();
				if(dest.isApplicable())destins.add(dest);
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private Player target;

	private int speed = 1;
	private int count = 0;
	private int index = 0;

	private boolean running = true;
	public GuiRoue(Player player, Player target) {
		super(RDD.get(), player, "Roue Du destin", 3*9, 1);


		this.target = target;
	}

	@Override
	public void update() {
		//fill background
		for(int i = 0; i < inv.getSize();i++){
			inv.setItem(i, new ItemFactory(Material.STAINED_GLASS_PANE).withName(" ").withColor(DyeColor.WHITE).done());
		}


		for(int i = -4; i <= 4; i++){
			inv.setItem(9 + i + 4, new ItemFactory(getDestin(index+i).getItem()).withName(getDestin(index+i).getName()).done());
		}

		if(count > 0){
			count--;
			return;
		}else{
			switch(speed){
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				speed++;
				count = 5;
				index++;
				break;
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				speed++;
				count = 15;
				index++;
				break;
			case 16:
			case 17:
				speed++;
				index++;
				if(running){
					running = false;
					finish();
				}
				break;
			}



		}
	}

	private void finish(){
		System.out.println("finish");
		
		if(target.isOnline()){
			Destin d = getDestin(index);

			Bukkit.broadcastMessage(RDD.get().getPrefix() + "Le joueur " + ChatColor.GOLD + target.getName() + ChatColor.GREEN + " a eu le destin " + ChatColor.GOLD + d.getName());
			Bukkit.broadcastMessage(RDD.get().getPrefix() + ChatColor.GOLD + d.getName() + ChatColor.GREEN + " -> " + d.getDesc());
			
			d.apply(target);
			

		}
	}


	private Destin getDestin(int index){
		//if index too hight, lower it
		while(index > destins.size()-1)index -= destins.size();
		while(index < 0)index += destins.size();

		return destins.get(index);


	}

}
