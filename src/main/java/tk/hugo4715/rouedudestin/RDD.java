package tk.hugo4715.rouedudestin;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import tk.hugo4715.rouedudestin.cmd.RDDCmd;
import tk.hugo4715.rouedudestin.config.RDDConfig;

public class RDD extends JavaPlugin {
	
	private RDDConfig config;

	private Map<Player, Player> targets = new HashMap<>();
	private String prefix = ChatColor.GRAY + "[" + ChatColor.GOLD + "RoueduDestin" + ChatColor.GRAY + "]" +  ChatColor.GREEN;

	@Override
	public void onEnable() {
		try {
			this.config = new RDDConfig();
		} catch (Exception e) {
			e.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		getServer().getPluginCommand("rouedudestin").setExecutor(new RDDCmd());
	}
	
	
	public Map<Player, Player> getTargets() {
		return targets;
	}
	
	/**
	 * change the target of this player
	 * @param p
	 */
	public void changeTarget(Player p){
		targets.remove(p);
		
		//remove players in spectator or creative
		List<Player> players = Bukkit.getOnlinePlayers()
				.stream()
				.filter(i -> !i.getName().equals(p.getName()) && i.getGameMode().equals(GameMode.ADVENTURE) || i.getGameMode().equals(GameMode.SURVIVAL))
				.collect(Collectors.toList());
		Collections.shuffle(players);
		
		if(!players.isEmpty()){
			//put random target in map
			targets.put(p, players.get(0));
		}
		
	}
	
	
	public String getPrefix() {
		return prefix;
	}
	
	public RDDConfig getconfiguration(){
		return config;
	}
	
	public static RDD get(){
		return getPlugin(RDD.class);
	}
}
