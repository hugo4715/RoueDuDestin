package tk.hugo4715.rouedudestin.destin;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class Destin16Gap extends Destin {

	private ItemStack item;
	public Destin16Gap() {
		item = new ItemFactory(Material.GOLDEN_APPLE).withAmount(16).done();
	}
	
	@Override
	public String getName() {
		return "16 Pommes dor�es";
	}

	@Override
	public void apply(Player target) {
		
		
		if(target.getInventory().firstEmpty() < 0){
			//no slot available, drop it
			target.getWorld().dropItem(target.getEyeLocation(), item);
		}else{
			target.getInventory().addItem(item);
		}
	}

	@Override
	public String getDesc() {
		return "Donne 16 pommes dor�es";
	}

	@Override
	public boolean isApplicable() {
		return RDD.get().getconfiguration().SC_16GAP;
	}

	@Override
	public ItemStack getItem() {
		return item;
	}
}
