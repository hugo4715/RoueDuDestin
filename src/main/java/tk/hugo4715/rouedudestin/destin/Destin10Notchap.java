package tk.hugo4715.rouedudestin.destin;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class Destin10Notchap extends Destin {

	private ItemStack item;

	public Destin10Notchap() {
		item = new ItemFactory(Material.GOLDEN_APPLE).withAmount(10).done();
		MaterialData md = item.getData();
		md.setData((byte) 1);
		item.setData(md);
	}
	
	@Override
	public String getName() {
		return "10 Pommes de Notch";
	}

	@Override
	public void apply(Player target) {
		
		
		
		if(target.getInventory().firstEmpty() < 0){
			//no slot available, drop it
			target.getWorld().dropItem(target.getEyeLocation(), item);
		}else{
			target.getInventory().addItem(item);
		}
	}

	@Override
	public String getDesc() {
		return "Donne 10 pommes de Notch";
	}

	@Override
	public boolean isApplicable() {
		return RDD.get().getconfiguration().SC_10NOTCHAP;
	}

	@Override
	public ItemStack getItem() {
		return item;
	}
}
