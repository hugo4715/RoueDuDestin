package tk.hugo4715.rouedudestin.destin;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class DestinHalfHealth extends Destin {

	private ItemStack item;
	
	public DestinHalfHealth() {
		item = new ItemFactory(Material.IRON_HOE).withAmount(1).done();
	}
	
	@Override
	public String getName() {
		return "Faucheuse";
	}

	@Override
	public void apply(Player target) {
		target.setHealth(Math.min(1, target.getHealth()/2));
	}

	@Override
	public String getDesc() {
		return "La mort vous enl�ve la moiti� de votre vie";
	}

	@Override
	public boolean isApplicable() {
		return RDD.get().getconfiguration().SC_HALF_HEALTH;
	}
	
	@Override
	public ItemStack getItem() {
		return item;
	}
}
