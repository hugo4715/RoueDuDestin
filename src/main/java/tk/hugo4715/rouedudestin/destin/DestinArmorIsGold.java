package tk.hugo4715.rouedudestin.destin;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class DestinArmorIsGold extends Destin {

	@Override
	public String getName() {
		return "Armure dor�e";
	}

	@Override
	public void apply(Player target) {
		PlayerInventory i = target.getInventory();
		i.setHelmet(new ItemFactory(Material.GOLD_HELMET).done());
		i.setChestplate(new ItemFactory(Material.GOLD_CHESTPLATE).done());
		i.setLeggings(new ItemFactory(Material.GOLD_LEGGINGS).done());
		i.setBoots(new ItemFactory(Material.GOLD_BOOTS).done());
	}

	@Override
	public String getDesc() {
		return "Remplace son armure par une en or";
	}

	@Override
	public boolean isApplicable() {
		return RDD.get().getconfiguration().SC_ARMOR_IS_GOLD;
	}

	@Override
	public ItemStack getItem() {
		return new ItemFactory(Material.GOLD_CHESTPLATE).done();
	}
}
