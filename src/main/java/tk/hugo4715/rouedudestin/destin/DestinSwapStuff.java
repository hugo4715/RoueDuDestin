package tk.hugo4715.rouedudestin.destin;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tk.hugo4715.rouedudestin.RDD;
import tk.hugo4715.rouedudestin.util.ItemFactory;

public class DestinSwapStuff extends Destin {

	private ItemStack item;
	
	public DestinSwapStuff() {
		item = new ItemFactory(Material.ENDER_PORTAL_FRAME).withAmount(1).done();
	}
	
	@Override
	public String getName() {
		return "Swap";
	}

	@Override
	public void apply(Player target) {
		List<Player> a = Bukkit.getOnlinePlayers().stream()
				.filter(p -> p.equals(target) && target.getGameMode().equals(GameMode.SURVIVAL) || target.getGameMode().equals(GameMode.ADVENTURE))
				.collect(Collectors.toList());
		
		Collections.shuffle(a);
		
		if(a.isEmpty()){
			Bukkit.broadcastMessage(RDD.get().getPrefix() + "Pas assez de joueurs");
		}else{
			Player swap = a.get(0);
		}
	}

	@Override
	public String getDesc() {
		return "Echange votre inventaire avec un autre joueur";
	}

	@Override
	public boolean isApplicable() {
		return RDD.get().getconfiguration().SC_SWAP_STUFF;
	}
	
	@Override
	public ItemStack getItem() {
		return item;
	}
}
