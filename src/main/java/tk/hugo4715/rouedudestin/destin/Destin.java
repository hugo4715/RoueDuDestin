package tk.hugo4715.rouedudestin.destin;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Destin {
	
	public enum Type{
		GAPx16(Destin16Gap.class),
		GAPx1(Destin1Gap.class),
		NOTCHx1(Destin1Notchap.class),
		NOTCHx10(Destin10Notchap.class),
		ARMOR_IS_GOLD(DestinArmorIsGold.class),
		HALF_HEALTH(DestinHalfHealth.class);
		
		private Class<? extends Destin> clz;

		private Type(Class<? extends Destin> clz) {
			this.clz = clz;
		}
		
		public Class<? extends Destin> getclz(){
			return clz;
		}
	}
	
	public abstract String getName();
	
	public abstract void apply(Player target);
	
	public abstract String getDesc();
	
	
	public boolean isApplicable(){
		return true;
	}
	
	public abstract ItemStack getItem();	
}
