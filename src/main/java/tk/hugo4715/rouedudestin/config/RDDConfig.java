package tk.hugo4715.rouedudestin.config;

import java.io.File;

import tk.hugo4715.rouedudestin.RDD;

public class RDDConfig {

	private static File file = new File(RDD.get().getDataFolder(), "config.yml");
	
	@ConfigItem(path="scenarios.half-health")
	public Boolean SC_HALF_HEALTH = true;
	
	@ConfigItem(path="scenarios.16-golden-apple")
	public Boolean SC_16GAP = true;
	
	@ConfigItem(path="scenarios.1-golden-apple")
	public Boolean SC_1GAP = true;
	
	@ConfigItem(path="scenarios.1-notch-apple")
	public Boolean SC_1NOTCHAP = true;
	
	@ConfigItem(path="scenarios.10-notch-apple")
	public Boolean SC_10NOTCHAP = true;
	
	@ConfigItem(path="scenarios.armor-is-gold")
	public Boolean SC_ARMOR_IS_GOLD = true;
	
	@ConfigItem(path="scenarios.swap-stuff")
	public Boolean SC_SWAP_STUFF = true;
	
	
	
	public RDDConfig() throws Exception{
		RDD.get().getLogger().info("Loading config file...");
		new ConfigLoader(this, file).load();
		
		
		RDD.get().getLogger().info("Loaded config file...");
	}
	
	public void save() throws Exception{
		RDD.get().getLogger().info("Loading config file...");
		new ConfigLoader(this, file).save();
		
		RDD.get().getLogger().info("Loaded config file...");
	}
	
}
