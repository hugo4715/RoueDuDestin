package tk.hugo4715.rouedudestin.config;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.commons.lang.Validate;
import org.bukkit.configuration.file.YamlConfiguration;

import net.md_5.bungee.api.ChatColor;

public class ConfigLoader {
	private Object target;
	private File file;

	public ConfigLoader(Object target, File file) {
		Validate.notNull(target);
		Validate.notNull(file);

		this.target = target;
		this.file = file;
	}

	public void load() throws IllegalArgumentException, IllegalAccessException, IOException {
		YamlConfiguration config;

		if (file.exists()) {
			config = YamlConfiguration.loadConfiguration(file);
		} else {
			config = new YamlConfiguration();
		}

		// load all field with annoation ConfigItem
		for (Field f : target.getClass().getDeclaredFields()) {
			ConfigItem i = f.getAnnotation(ConfigItem.class);

			if (i != null) {
				f.setAccessible(true);// private fields bypass
				if (config.contains(i.path())) {
					System.out.println("config already contains " + i.path() + " with value " + config.get(i.path()));
					// set the field value, replacing color codes if it is a
					// string
					f.set(target,
							config.isString(i.path())
									? ChatColor.translateAlternateColorCodes('&', config.getString(i.path()))
									: config.get(i.path()));
				} else {
					System.out.println("config does not contain  " + i.path());
					config.set(i.path(), f.get(target));
				}
			}
		}

		config.save(file);
	}

	public void save() throws IllegalArgumentException, IllegalAccessException, IOException {
		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		// load all field with annoation ConfigItem
		for (Field f : target.getClass().getDeclaredFields()) {
			ConfigItem i = f.getAnnotation(ConfigItem.class);

			if (i != null) {
				f.setAccessible(true);// private fields bypass
				if (config.contains(i.path())) {
					f.set(target, config.get(i.path()));
				} else {
					config.set(i.path(), f.get(target));
				}
			}
		}

		config.save(file);
	}

}