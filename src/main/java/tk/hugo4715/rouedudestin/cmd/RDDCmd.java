package tk.hugo4715.rouedudestin.cmd;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tk.hugo4715.rouedudestin.gui.RDDGui;

public class RDDCmd implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if(!(sender instanceof Player))return false;
		
		Player p = (Player)sender;
		
		if(args.length == 0){
			//Gui
			
			if(p.hasPermission("rdd")){
				new RDDGui(p);
			}else{
				//hide cmd
				return false;
			}
			
		}
//		else if(args.length >= 1 && args[0].equalsIgnoreCase("setup") || args[0].equalsIgnoreCase("settings")){
//			//TODO Settings gui
//		}
		
		
		return false;
	}

}
